export default {
  // Uncomment to enable dark theme
  //dark: true,
  themes: {
    light: {
      primary: '#00386e',
      accent: '#116e8b',
      secondary: '#a5d027',
      success: '#00D28C',
      info: '#70706f',
      warning: '#FF641E',
      error: '#F03C0E'
    },
    dark: {
      primary: '#00386e',
      accent: '#116e8b',
      secondary: '#a5d027',
      success: '#00D28C',
      info: '#70706f',
      warning: '#FF641E',
      error: '#F03C0E'
    }
  }
}
