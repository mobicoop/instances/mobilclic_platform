

blueDark:=$(shell tput setaf 21)
blue:=$(shell tput setaf 33)

ifeq ($(shell uname),Darwin)
  os=darwin
else
  os=linux
endif

install:
	$(info $(blue)Creating build/cache folders$(reset))
	@mkdir -p build/cache ;\

	$(info $(blue)Creating build/cache folders$(reset))
	@mkdir -p build/cache;\

	$(info $(blue)------------------------------------------------------)
	$(info $(blue)mobilclic_platform ($(os)): Installing node deps...)
	$(info $(blue)------------------------------------------------------$(reset))

	@docker-compose -f docker-compose-builder-$(os).yml run --rm install
	@make -s install-vendor

install-vendor:

	$(info $(blue)------------------------------------------------------)
	$(info $(blue)mobilclic_platform ($(os)): Installing php deps...)
	$(info $(blue)------------------------------------------------------$(reset))

	@docker-compose -f docker-compose-builder-$(os).yml run --rm install-vendor


fixtures:
	$(info $(blue)------------------------------------------------------)
	$(info $(blue)mobilclic_platform ($(os)): Generating fixtures...)
	$(info $(blue)------------------------------------------------------$(reset))
	@docker-compose -f docker-compose-builder-$(os).yml run --rm fixtures

start:
	$(info mobilclic_platform ($(os)): Starting mobilclic_platform environment containers.)
	@docker-compose -f docker-compose-$(os).yml up -d
 
stop:
	$(info mobilclic_platform ($(os)): Stopping mobilclic_platform environment containers.)
	@docker-compose -f docker-compose-$(os).yml stop 

status:
	@docker ps -a | grep mobilclic_platform_platform
	@docker ps -a | grep mobilclic_platform_db
 
restart:
	$(info mobilclic_platform ($(os)): Restarting mobilclic_platform environment containers.)
	@make -s stop
	@make -s start

reload:
	$(info Make ($(os)): Restarting mobilclic_platform environment containers.)
	@make -s stop
	@make -s remove
	@make -s start

remove:
	$(info mobilclic_platform ($(os)): Stopping mobilclic_platform environment containers.)
	@docker-compose -f docker-compose-$(os).yml down -v 
 
clean:
	@make -s stop
	@make -s remove
	$(info $(blue)------------------------------------------------------)
	$(info $(blue)Drop all deps + containers + volumes)
	$(info $(blue)------------------------------------------------------$(reset))
	sudo rm -rf node_modules vendor

logs: 
	$(info $(blueDark)------------------------------------------------------)
	$(info $(blueDark)mobilclic_platform+DB Logs)
	$(info $(blueDark)------------------------------------------------------$(reset))
	@docker logs -f mobilclic_platform

go-platform:
	@docker exec -it mobilclic_platform zsh